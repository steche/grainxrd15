#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import fabio
import numpy as np
from ImageD11 import columnfile
import matplotlib.pyplot as plt

def sortedscans( hfo ):
    return [sc for _,sc in sorted([(float(s),s) for s in list(hfo)])]

class eiger_spatial(object):
    def __init__(self,
                 dxfile="/data/id15/inhouse6/ihhc3673/id15/processed/pilatus_dx_2020.edf",
                 dyfile="/data/id15/inhouse6/ihhc3673/id15/processed/pilatus_dy_2020.edf"):
        self.dx = fabio.open(dxfile).data
        self.dy = fabio.open(dyfile).data
    def __call__(self, pks):
        si = np.round(pks['s_raw']).astype(int)
        fi = np.round(pks['f_raw']).astype(int)
        pks['sc'] = self.dy[ si, fi ] + pks['s_raw']
        pks['fc'] = self.dx[ si, fi ] + pks['f_raw']
        return pks

def tocolf(pks):
    titles = list(pks.keys())
    colf = columnfile.newcolumnfile( titles=titles )
    nrows = len(pks[titles[0]])
    colf.nrows = nrows
    colf.set_bigarray( [ pks[t] for t in titles ] )
    return colf


def newfig():
    plt.figure(dpi=150, figsize=(6,6))

    
def get_yo_bins(colf, ystep, ostep, show=False):
    """
    input: a columnfile instance, y-step (float), omega-step (float)
    output: y bins, y bin centers, omega bins, omega bin centers
    """
    dtys = sorted(np.unique(colf.dty))
    ymin, ymax = min(dtys), max(dtys)
    ybins = np.arange(min(dtys)-ystep/2, max(dtys)+ystep/2, ystep)
    ycens = (ybins[1:] + ybins[:-1])/2

    omega_digits = 4
    omegas = sorted(np.unique(np.round(colf.omega, omega_digits)))
    omin, omax = min(omegas), max(omegas)
    obins = np.round(np.arange(omin-ostep/2, max(omegas)+ostep/2, ostep), omega_digits)
    ocens = (obins[1:] + obins[:-1])/2

    if show is True:
        print('dtys:', ybins[:3],'...',ybins[-3:])
        print('ybins:', ybins[:3],'...',ybins[-3:])
        print('ycens:', ycens[:3],'...',ycens[-3:])
        print('omegas:', omegas[:3],'...',omegas[-3:])
        print('obins:', obins[:3],'...',obins[-3:])
        print('ocens:', ocens[:3],'...',ocens[-3:])

    return ybins, ycens, obins, ocens