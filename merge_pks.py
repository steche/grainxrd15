import h5py
import numpy as np
from ImageD11 import columnfile

# import all files
c1 = columnfile.columnfile('file1.h5')
c2 = columnfile.columnfile('file2.h5')

# use c1 as destination. this will not overwrite anything on disk
c1.set_bigarray(np.concatenate([c1.bigarray, c2.bigarray], axis=1))

# write to disk
with h5py.File('file_merged.h5', 'w') as outfile:
    columnfile.colfile_to_hdf(c1, outfile)
