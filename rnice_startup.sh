echo "this scripts allocates resources using salloc and then goes to the data folder"
echo "copy this into your rnice home folder and make sure it's executable (chmod +x rnice_startup.sh)"

salloc --chdir /data/visitor/hc4778/id15a/20220705/processed --x11 --ntasks=1 --cpus-per-task=10 --mem-per-cpu=4G --time=6:00:00 srun --pty bash 
