# Contents
- [Analysis using ImageD11](#analysis)<br>
  * [Before you start](#an0)<br>
  * [Segmentation and peak search](#an1)<br>
  * [Indexing and refining](#an2)<br>
  * [Mapping indexed peaks to grains](#an3)<br>
- [Sample list](#samplelist)



# Analysis using ImageD11 <a name="analysis"></a>
## 0) Before you start <a name="an0"></a>
### Paths </tt> <a name="paths"></a>
Because raw and processed data are large (100s of GB) they have to be stored in the original experiment folder, not in your personal home folder. However, most scripts will be stored in your home folder for easy access and any code will simply point to the *visitor* disk.
|  |  |
| ------ | ------ |
| Raw data | <tt>/data/visitor/hc4778/id15a/20220705/</tt> |
| Processed data | <tt>/data/visitor/hc4778/id15a/20220705/processed* |
| Your analysis scripts / home | <tt>/home/esrf/yourname/yourscripts/</tt> |
| ImageD11 executable  | <tt>/home/esrf/yourname/.local/bin/</tt> |

### Work environments: <a name="envs"></a>
#### jupyter-slurm 
IDE for working on notebooks and scripts, submitting jobs to computing cluster. <br>
Login to `jupyter-slurm.esrf.fr` and select resources as needed <br>
Work directory: your [home](#paths) <br>
#### nice/rnice
ESRF's default desktop/terminal workspace. Does not support heavy tasks unless they are sent to the computing cluster [(info)](https://confluence.esrf.fr/display/SCKB/Job+scheduling+and+managing+computer+resources+with+SLURM)<br>
Login to `remote.esrf.fr` and select Compute Cluster, then one of the *slurm-nice-devel* nodes <br>
Execute the [startup code](https://gitlab.esrf.fr/steche/grainxrd15/-/blob/aff38eb5ea93d11ce2f5024b6201226b6d154ad7/rnice_startup.sh), or manually allocate computing resources:
```
salloc --x11 --ntasks=1 --cpus-per-task=10 --mem-per-cpu=4G --time=6:00:00 srun --pty bash
```
Work directory: your [home](#paths). It could be convenient to change directory to the [processed data dir](#paths) <br>
In theory `visa.esrf.fr` should be equivalent
#### lid15agpu1
ID15 'performance' machine, terminal-only but can launch GUIs. Supports heavy tasks for a short time (e.g. peak indexing)  <br>
Login as `opid15@lid15agpu1` and change directory to where the [data are stored](#paths)


### Install ImageD11
From your *rnice* environment, try running `ImageD11_gui.py`. If the file does not exist, install ImageD11 and all dependencies locally:
```
/usr/bin/python3 -m pip install --user ImageD11[Full]
```
If the file `~/.local/bin/ImageD11_gui.py` exists but there's an error message about the Python interpreter, edit the first line of the file  (or a copy) specifying an existing python interpreter. Also make sure that the [folder containing the executable](#paths) is in the linux PATH.



## 1) Segmentation and peak search <a name="an1"></a>
### Files needed:
* 01_segmentation_slurm.ipynb (editable)
* segmenter.py (editable)
* pfun.py (source only)
* base.py (source only)

### Environment: [jupyter-slurm](#envs)
* Login to `jupyter-slurm.esrf.fr` and start a session with 10 cores and 12 hours walltime
* Your working directory should be located in your [home directory](#paths)
* To keep track of all the sample-dependent (and attempt-dependent) changes it will be convenient to have a separate folder for each sample, even if this means repeating some of the code. This can be optimised in the future. 
* The folder should look like this: 
```
 /home/esrf/yourname/3dxrd/sample1/
 |-- 01_segmentation_slurm.ipynb
 |-- segmenter.py
 |-- pfun.py
 |-- base.py
```

### Set input files and parameters
* Open the notebook `01_segmentation_slurm.ipynb`
* In the first cell set the filenames containing your raw diffraction data and check they exist
* The second cell contains commands to install python packages that may be missing. Uncomment those lines if needed
* In the third cell you can edit resource parameters related to cluster allocation. The default values can process an average scan in about 20 minutes and that's fine
* Open the script `segmenter.py`
* The parameters mostly dependent on sample and scan type are `CUT`, `THRESHOLDS`, `PIXELS_IN_SPOT`, `MASKFILE`. Set them as described in the file
* Set `OUTPATH` to an empty folder in the [processed data path](#paths)
* Check at the end of the file that both the functions `segment()` and `topks()` are included in the main execution

### Run the code
* Go back to the notebook `01_segmentation_slurm.ipynb`
* Run all cells until the last one. What they do is to prepare the instructions file to submit the segmentation/peaksearch job to the computing cluster, and to show the output
* To submit the job, execute the last cell. A message will remind you to check the output logs

### Check the output
* The script will create an output folder called *slurm* storing all error and standard output from the jobs submitted. These logs can be checked at any time while the code is running. The folder will then look like this:
```
 /home/esrf/yourname/3dxrd/sample1/
 |-- slurm
     |-- arrayJob_3943813_1.err
     |-- arrayJob_3943813_1.out
 |-- 01_segmentation_slurm.ipynb
 |-- segmenter.py
 |-- pfun.py
 |-- base.py
```
* If segmentation and peak-search complete successfully, your [output directory](#paths) will contain two new files:
```
/data/visitor/hc4778/id15a/20220705/processed/sample1/
|-- sample1_sparse.h5
|-- sample1_sparse_pks.h5
```
* The file <tt>*sparse.h5</tt> is the sparse representation of the diffraction images after masking and thresholding. It will only be used again to perform a peak search with different parameters, if necessary
* The file <tt>*sparse_pks.h5</tt> contains coordinates (in sample space and detector space) and intensities of the peaks extracted. This is the file used for further analysis. It can be loaded in the ImageD11 gui (<tt>Transformation/Load Filtered Peaks</tt>) or processed with the ImageD11 API (`ImageD11.columnfile.columnfile(filename)`)


## 2) Indexing and refining <a name="an2"></a>

### Files needed:
* sample1_sparse_pks.h5 (source only; to use multiple files see [this script](https://gitlab.esrf.fr/steche/grainxrd15/-/blob/804e9d6885786c5575eb6a1126231b3d0805a791/merge_pks.py))
* initial.par (editable)

### Environment: [rnice](#envs) or [lid15agpu1](#envs) <a name="indenv"></a>
* Login to lid15agpu1 or to rnice/slurm-devel. On *rnice* [allocate some resources](#envs)
* Change directory to the [experiment folder](#paths)
* Launch `ImageD11_gui.py`

### Load peaks and refine geometry <a name="loadpeaks"></a>
* All the commands here are located in the *Transformation* menu of the GUI
* Select *Load filtered peaks* to import your *_pks* file
* Then select *Load parameters* if you've got a [parameter file](https://gitlab.esrf.fr/steche/grainxrd15/-/blob/b8add1ac775d5bc4a683ceff3eb57794f2ac760b/initial_BFO.par), or *Edit parameters* to set them from scratch and save them later. Note:
   - `distance`, `y_size`, and `z_size` are in microns
   - `y_center` and `z_center` are in pixels
   - To add the space group, open the paramters file in an editor and add the handle `cell_sg R3c`
* *Plot x/y* shows all peaks from all frames in detector space; *Plot tth/eta* maps them to polar coordinates
* *Add unit cell peaks* will compare experimental peak positions with theoretical, both being based on the cell parameters and detector geometry parameters provided
* Manually set parameters to a reasonable first guess, then select *Transformation/Fit* to refine. Iterate until reaching a good agreement
* To map the observed peak positions onto scattering vectors, select *Compute g-vectors* and then *Save g-vectors*. The output folder will simply be:
```
/data/visitor/hc4778/id15a/20220705/processed/sample1/
|-- sample1_sparse.h5
|-- sample1_sparse_pks.h5
|-- sample1_pks.gv
```

### Indexing <a name="index"></a>
* All the commands here are located in the *Indexing* menu of the GUI
* *Load g-vectors* to recall the <tt>.gv</tt> file previously saved
* *Assign powder rings* lists all available hkls and their candidate peaks, based on the parameters above 
<details><summary>(show output)</summary>
<p>

```
Ring 10  ( -2,  0,  0)   30    852       0      852    15     56  4.43
Ring 9   ( -1, -1, -2)   12     35       0       35     1     22  4.13
Ring 8   (  0,  0, -5)   14  434606       0    434606 16531     26  3.86
Ring 7   ( -1,  0, -4)   18  603427       0    603427 17852     33  3.80
Ring 6   (  0, -1, -3)   12    736       0      736    32     22  3.20
Ring 5   (  0,  0, -4)    2    349       0      349    92      3  3.09
Ring 4   (  0, -1, -2)   12  269820       0    269820 11974     22  2.70
Ring 3   (  0,  0, -3)   14      8       0        8     0     26  2.32
Ring 2   ( -1,  0,  0)    6     11       0       11     0     11  2.21
Ring 1   (  0,  0, -2)    2     19       0       19     5      3  1.54
Ring 0   (  0,  0, -1)    2      2       0        2     0      3  0.77
```

</p>
</details>

* Edit and then save the indexing parameters: ([documentation](https://imaged11.readthedocs.io/en/latest/indexing.html))
  - max_grains: extract only this many grains
  - minpks: each grain-defining orientation matrix will need at least this many indexed peaks
  - ring_1: ring chosen for indexing/orientation searching; better if it's highly populated and not overlapping with too many others
  - ring_2: second ring chosen for indexing; can be the same as ring_1
  - uniqueness: 0-1 fraction of peaks that distinct grains are allowed to share
  - hkl_tol: tolerance of indexed == | h-int(h) | < hkl_tol
  - ds_tol: margin of error on the reciprocal *d* to increase/reduce the bin size of each ring. It should be checked against the ring list to make sure all strong reflections are considered: too low --> high peak rejection; too high --> hkls overlap, wrong assignment

* *Generate trial orientations* generates a list of orientation matrices (UBIs) for each g-vector. The output should look like this:
<details><summary>(show output)</summary>
<p>

```
DEBUG    : Running: myindexer.scorethem( )

INFO     : Scoring 165351 potential orientations
INFO     : new grain 4135 pks, i 252075 j 272814 UBI [  0.06922346   4.01662411   3.92262999  -0.9022406    1.34474057  -5.37205962 -13.72482905  -1.62714761   1.89365342 ]
INFO     : new grain 2359 pks, i 252072 j 294334 UBI [  1.29114993  -4.88967003   2.41773891  -1.40481337   4.44486045  3.1757817  -13.4147192   -3.77351476  -0.53803581]
INFO     : new grain 3444 pks, i 252067 j 312193 UBI [ 0.16115688 -5.48946406  1.15308358 -1.19655261  1.7416284  -5.20044791 13.57704453 -0.28317451 -3.21637299]
INFO     : new grain 2818 pks, i 252060 j 304124 UBI [-0.19483148  0.02100912  5.60860561  3.82362735  3.11538118 -2.69356176 -8.92330251 10.64913952 -0.40443845]
INFO     : new grain 1294 pks, i 252058 j 397828 UBI [ 6.37002918e-03 -4.94307221e+00 -2.68679956e+00 -4.63931334e+00  3.16930588e+00  7.57130101e-02  4.21990083e+00  6.36274502e+00 -1.17003964e+01]
INFO     : new grain 7395 pks, i 252054 j 338183 UBI [  0.17061385  -3.17350437   4.62979891  -0.92412926   5.52133098   0.41306043 -13.73561438  -2.22601463  -1.02210375]
INFO     : new grain 2384 pks, i 252052 j 260284 UBI [ 5.59268364 -0.11159647  0.47273312 -2.42317037 -1.29968939 -4.89265062  0.59359368 13.39934566 -3.85022561]
INFO     : new grain 1051 pks, i 252051 j 380845 UBI [ 2.06013059 -2.55720944 -4.56658991 -4.18968724  3.71021924 -0.4959082  9.29427614 10.25319154 -1.58514913]
INFO     : new grain 3242 pks, i 252050 j 354596 UBI [-4.42190641 -1.08558435 -3.28251389  2.80416441 -4.22058914  2.41584766 -8.41571404  0.7473897  11.09909053]
INFO     : new grain 1636 pks, i 252042 j 274823 UBI [ 5.04823186 -0.77565318 -2.3242766  -4.56462682  0.54744959 -3.2746173  1.96792227 13.77330079 -0.4141417 ]
INFO     : Number of orientations with more than 1000 peaks is 10
INFO     : Time taken 1.607/s
INFO     : UBI for best fitting
[[  0.17061385  -3.17350437   4.62979891]
 [ -0.92412926   5.52133098   0.41306043]
 [-13.73561438  -2.22601463  -1.02210375]]
INFO     : Unit cell: (5.615627923394632, 5.61335279814052, 13.952309469027744, 90.01409803164141, 90.00830259205907, 120.0125939572501)

INFO     : Indexes 8967 peaks, with <drlv2>=0.008657
INFO     : That was the best thing I found so far
INFO     : Number of peaks assigned to rings but not indexed = 1503660
```

</p>
</details>

* To get an idea of how closely the fitted UBIs match the indexed peaks, select *Histogram fit quality*. [The plot](https://gitlab.esrf.fr/steche/grainxrd15/-/blob/618ad3fb7fb1c3a72db3a3b1faa6122f9c1881ba/.other/drlv_histogram.PNG) shows the number of indexed peaks as a function of the residual hkl. A maximum at 0.01-0.04 should indicate successful indexing. The slope afterwards represents rejected peaks.


* *Save UBI matrices* saves a text file (let's call it `.ubi`) with a UBI matrix for each of the grains found so far

* *Write indexed peaks* saves an updated version of the peak file (let's call it `.idx`), in column format, where each indexed peak is assigned to a grain (grain labels from 0 to max n of grains) and to a set of fitted Miller indices. The output folder will now contain:
```
/data/visitor/hc4778/id15a/20220705/processed/sample1/
|-- sample1_sparse.h5
|-- sample1_sparse_pks.h5
|-- sample1_pks.gv
|-- sample1_indexing.pars
|-- sample1_pks.ubi
|-- sample1_pks.idx
```

### Alternative indexing using jupyter notebook
* notebook: [<tt> 02_indexing.ipynb </tt>](https://gitlab.esrf.fr/steche/grainxrd15/-/blob/main/part2/02_indexing.ipynb)
* pros:
    - filtering based on intensity, 2theta range, and agreement with theoretical 2theta should be easier
    - g-vectors calculated on the optimised peak list are quietly passed to the indexer
    - multiple ring selection in order to extract more UBIs (not always an advantage)
* cons:
    - no "live" overview plot, has to be manually scripted every now and then
    - no popup menus to select files; one of the first few cells controls all the filenames


## 3) Mapping indexed peaks to grains <a name="an3"></a>

### Files needed:
* sample1_sparse_pks.h5
* initial.par
* sample1_pks.ubi
* [03_mapping.ipynb](https://gitlab.esrf.fr/steche/grainxrd15/-/blob/main/part3/03_mapping.ipynb)

### Environment: [rnice](#envs) or [lid15agpu1](#envs)
* If you're just done indexing, keep the same environment open
* Otherwise [repeat the same steps](#indenv)

### Run makemap.py
```
makemap.py -p initial.par -u sample1_pks.ubi -U sample1.ubi.map -f sample1_sparse_pks.h5 -l trigonalH -t 0.02 --omega_slop 0.05
```
Note:
- the parameter file (`-p`) is the one containing cell geometry and detector geometry
- the input ubi file (`-u`) is the output of *Save UBI matrices* in the previous section
- the output ubi file (`-U`) should be a new file in the first iteration of *makemap*. This output contains refined UBI values, each matrix associated with grain information: translation from the center (units?), 
- the input peak file (`-f`) could be almost anything? Better using the original filtered file (documentation seems to suggest so), the after-indexing column file or the output of a previous run of *makemap*?
- there is also *fitgrain* but often it does not converge

### Get spatial reconstruction of individual grains
* Change environment to [jupyter-slurm](#envs)
* Open the notebook [03_mapping.ipynb](https://gitlab.esrf.fr/steche/grainxrd15/-/blob/main/part3/03_mapping.ipynb)
* Load the peaks output of *makemap* as *pksname*, the usual parameter file with unit cell and geometry as *parname*, and the output ubi of *makemap* as *grainsname*
* The last block of code plots the sinogram and reconstruction of the peaks indexed within each grain



## 4) Assembling model and strain calculations <a name="an4"></a>
This should be the goal

# Sample list <a name="samplelist"></a>
#### BFO
* sample: conical BFO machined onto pin, originally from Marta/John
* scan type: 170+170 degree rewind (fscan2d)
* path: ```BFO/BFO_000?/BFO_000?.h5```
#### CST take3 detached
* sample: CST measured overnight with applied field, though unsure about actual electric contact
* scan type: 360 degree continuous (aeroystepscan)
* path: ```CST/```
#### CST perpendicular 0 V 
* sample: last measured CST, ending saturday morning, confirmed electric contact
* scan type: 170+170 degree rewind (fscan2d)
* path: ```CST/```
#### CST perpendicular 100 V 
* sample: last measured CST, ending saturday morning, confirmed electric contact 
* field: one voltage applied before breakdown
* scan type: 170+170 degree rewind (fscan2d)
* path: ```CST/```
#### CST perpendicular last (final?)
* sample: last measured CST, ending saturday morning, confirmed electric contact 
* measured after possible electric breakdown, coarse+fine scan
* scan type: 170+170 degree rewind (fscan2d)
* path: ```CST/```
#### TTB 0 V 
* scan type: 170+170 degree rewind (fscan2d)
* path: ```TTB/```
